import { Router } from "express";
import routes from "./routes";
import add from "./add";
import read from "./read";

const rootRouter = Router();

rootRouter.post(routes.add.path, add);
rootRouter.get(routes.read.path, read);

export default rootRouter;
