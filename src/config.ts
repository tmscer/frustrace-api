import nconf from "nconf";
import path from "path";
import { TimeUnit } from "./db/counter.model";

// Setup nconf to use (in-order):
// 1. Command-line arguments
// 2. Environment variables
// 3. A file located at '/config/{NODE_ENV}.json'
// 4. Defaults
nconf.argv().env();
const env = process.env.NODE_ENV || "production";
nconf.file({
  file: path.join(__dirname, "..", "config", `${env}.json`)
});
nconf.defaults({
  mongo: {
    host: "127.0.0.1",
    port: 27017,
    db: "dev_frustrace"
  },
  counter: {
    // Resolution is 60 seconds
    unit: TimeUnit.SECOND.valueOf(),
    n: 60,
    // Every 10s
    updateCacheMs: 10 * 1000,
  },
  read: {
    // Return last 24 hours
    unit: TimeUnit.HOUR.valueOf(),
    n: 24,
    // Every 10s
    updateCacheMs: 10 * 1000,
  },
  server: {
    port: 8080
  },
  ping: true
});

export default nconf;
