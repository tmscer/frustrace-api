import { app, begin } from "./app";
import config from "./config";

const port = config.get("server:port");

begin().then(() => {
  app.listen(port, () =>
    console.log(`Frustrace API listening on port ${port}!`)
  );
});
