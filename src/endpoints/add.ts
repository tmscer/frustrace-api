import { Handler } from "../app";
import config from "../config";
import {
  Counter,
  CounterDocument
} from "../db/counter.model";

const {
  unit: resolutionUnit,
  n: resolutionN,
  updateCacheMs
} = config.get(
  "counter"
);

const updatesBeforeNew = (resolutionUnit * resolutionN) / updateCacheMs
// Memory cache
let currentCounter: CounterDocument = null;
let updateCounter = true;

const newCounter = (now: Date): CounterDocument => {
  return new Counter({
    start: now,
    end: new Date(now.getTime() + resolutionN * resolutionUnit),
    ips: {},
    hits: []
  });
};

const add: Handler = async (req, res, next) => {
  res.status(200).end();
  const now = new Date();
  let counter = currentCounter;
  if (counter === null || counter.end.getTime() <= now.getTime()) {
    if (counter !== null) {
      await counter.save();
    }
    currentCounter = await newCounter(now);
    counter = currentCounter;
  }
  counter.hits.push(now);

  if (updateCounter) {
    setTimeout(async () => {
      updateCounter = true;
      await currentCounter.save();
    }, updateCacheMs);
    updateCounter = false;
  }
  return next();
};

export default add;
