interface IRoute {
  name: RoutableEndpoints;
  path: string;
}

type RoutableEndpoints =
 "ping"
  | "add"
  | "read"

type RouteConfig = Record<RoutableEndpoints, Omit<IRoute, "name">>;

const config: RouteConfig = {
  ping: { path: "/ping" },
  add: { path: "/add" },
  read: { path: "/read" },
};

function getRoutes(
  routeConfig: RouteConfig
): Record<RoutableEndpoints, IRoute> {
  return Object.keys(routeConfig)
    .map(key => {
      return {
        name: key,
        ...routeConfig[key]
      };
    })
    .reduce(
      (a, c) => {
        a[c.name] = c;
        return a;
      },
      {} as any
    );
}

export default getRoutes(config);
