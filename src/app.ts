import express, { Request, Response, NextFunction } from "express";
import config from "./config";
import * as db from "./db";
import routes from "./endpoints/routes";
import rootRouter from "./endpoints/index";
import bodyParser from "body-parser";
import cors from "cors";
import { Params } from "express-serve-static-core";
import compression from "compression";

export type Handler<P extends Params = Params> = (
  req: Request<P>,
  res: Response,
  next: NextFunction
) => any;

const app = express();

const corsOptions = {
  // TODO: Move this to settings
  origin: "*"
};

// Compression
app.use(compression());
// CORS protection
app.use(cors(corsOptions));
// JSON parser
app.use(bodyParser.json());
// Mount the root router
app.use("/", rootRouter);

if (config.get("ping")) {
  app.get(routes.ping.path, (_, res) => {
    res.set("Content-Type", "text/plain");
    res.send("pong");
  });
}

const begin = async () => {
  await db.connect();
};

export { app, begin };
