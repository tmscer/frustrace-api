import mongoose from "mongoose";

export enum TimeUnit {
  SECOND = 1000,
  MINUTE = SECOND * 60,
  HOUR = MINUTE * 60,
  DAY = HOUR * 24
}

export interface ICounter {
  start: Date;
  end: Date;
  hits: Date[];
  ips: {
    [ip: string]: number
  };
}

export interface CounterDocument extends ICounter, mongoose.Document {}

export const CounterSchema = new mongoose.Schema({
  start: {
    type: Date,
    required: true
  },
  end: {
    type: Date,
    required: true,
  },
  hits: [Date],
  ips: {
    type: mongoose.Schema.Types.Map,
    of: Number,
  }
});

export const Counter = mongoose.model<CounterDocument>("Counter", CounterSchema);
