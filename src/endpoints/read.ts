import { Handler } from "../app";
import config from "../config";
import { Counter } from "../db/counter.model";

const { unit, n, updateCacheMs } = config.get("read");

let cachedResponse = null;
let updateCache: boolean = true

const createResponse = async (now: Date) => {
  const data = await Counter.aggregate([
    {$match: {
      end: { $gte: new Date(now.getTime() - unit * n) }
    }},
    {$project: {
      start: "$start",
      end: "$end",
      hitsN: {$size: "$hits"}
    }}
  ]);
  return {data:data.map(counter => [counter.start, counter.end, counter.hitsN])};
}

const read: Handler = async (req, res, next) => {
  const now = new Date();
  // Get all counters since unit * n ago
  if (cachedResponse === null || updateCache) {
    updateCache = false;
    cachedResponse = await createResponse(now);
    setTimeout(() => {
      updateCache = true;
    }, updateCacheMs);
  }
  res.json(cachedResponse);
  return next();
};

export default read;
