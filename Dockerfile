FROM node:13-buster-slim
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
COPY . /usr/src/app
RUN npm install
RUN npm run compile
EXPOSE 8080

ENTRYPOINT npm start
